import Beautymates from './src/App';
import Messaging from './src/Messaging';

export const MessagingPopUp = Messaging;

export default Beautymates;
