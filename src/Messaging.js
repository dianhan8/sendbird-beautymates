/* eslint-disable max-len */
/* eslint-disable new-cap */
import React from 'react';
import PropTypes, { array } from 'prop-types';
import classnames from 'classnames';
import { useForm } from 'react-hook-form';
import moment from 'moment';
import _ from 'lodash';
import Sendbird from 'sendbird';

import Preview from './components/Preview';
import MessageItem from './components/MessageItem';
import MessageImage from './components/MessageImage';
import Img from './components/Img';

/**
 *
 * @param {*} props
 * @return {function}
 */
function IsActive(props) {
  const { users } = props;

  return (
    _.get(users, 'connectionStatus') === 'offline' ? (
      <span className="text-xs text-gray-500 truncate" style={{ maxWidth: 100 }}>
        Offline
      </span>
    ) : (
        <div className="flex items-center">
          <span className="text-sm text-green-500 mr-1">
            <svg
              t="1595902622935"
              className="icon w-3 h-3 fill-current"
              viewBox="0 0 1024 1024"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              p-id="1523"
            >
              <path
                // eslint-disable-next-line max-len
                d="M512 298.666667c117.333333 0 213.333333 96 213.333333 213.333333s-96 213.333333-213.333333 213.333333-213.333333-96-213.333333-213.333333S394.666667 298.666667 512 298.666667z"
                p-id="1524"
              />
            </svg>
          </span>
          <span className="text-xs text-gray-500 font-medium">
            Active
          </span>
        </div>
      )
  );
}

IsActive.propTypes = {
  users: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

/**
 *
 * @param {{
 *  currentChannel: string,
 *  onClose: function,
 *  onMinimaze: fucntion,
 *  userIdStore: string,
 *  }} props
 * @return {function}
 */
function HeadMessaging(props) {
  const { currentChannel, onClose, onMinimaze, userIdStore } = props;
  const [users, setUsers] = React.useState(false);

  React.useEffect(() => {
    if (currentChannel !== null) {
      if (!users) {
        // eslint-disable-next-line max-len
        const finalResult = _.filter(currentChannel.members, function (memberItem, index) {
          return memberItem.userId == userIdStore;
        });
        setUsers(finalResult[0]);
      }
    }
  }, [currentChannel]);

  if (!currentChannel) {
    return (
      <div
        // eslint-disable-next-line max-len
        className="px-4 py-2 border-b border-gray-300 flex items-center justify-between focus:outline-none"
      >
        <div
          tabIndex={0}
          role="button"
          className="flex items-center focus:outline-none"
          onClick={() => onMinimaze()}
        >
          <div
            className="w-16 h-10 animated-background"
          />
          <div className="flex flex-col ml-2">
            <div className="animated-background h-4 mb-1 w-24" />
            <div className="animated-background h-2 w-10" />
          </div>
        </div>
      </div>
    );
  }

  return (
    <div
      // eslint-disable-next-line max-len
      className="px-4 py-2 border-b border-gray-300 flex items-center justify-between focus:outline-none"
    >
      <div
        tabIndex={0}
        role="button"
        className="flex items-center focus:outline-none"
        onClick={() => onMinimaze()}
      >
        <Img
          src={_.get(users, 'profileUrl')}
          className="w-16 h-10 object-cover"
          lazyload
        />
        <div className="flex flex-col ml-2">
          <span
            className="font-hind text-sm font-semibold truncate"
            style={{ maxWidth: 100 }}
          >
            {_.get(users, 'nickname')}
          </span>
          <IsActive
            users={users}
          />
        </div>
      </div>
      <div className="flex items-center">
        {/* <Dropdown
          triggerProp={
            <svg t="1598244673040" className="w-5 h-5" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2556" width="200" height="200"><path d="M511.94 896c-9.69 0-19.29-0.34-28.5-1.07-41.78-3.86-75.22-36.31-80.34-77.95l-3.03-20.95a43.96 43.96 0 0 0-24.28-30.17c-5.29-2.86-10.54-5.89-15.57-9.05a50.168 50.168 0 0 0-26.07-8.11c-4.17-0.05-8.31 0.71-12.2 2.22l-19.71 7.85a96.281 96.281 0 0 1-35.41 6.78 85.816 85.816 0 0 1-72.19-37.25 384.931 384.931 0 0 1-28.59-49.58c-17.53-38.1-6.1-83.27 27.43-108.46l16.68-13.23a43.745 43.745 0 0 0 14.04-36.01v-9.05-9.05a43.715 43.715 0 0 0-14.04-36.01l-16.68-13.23c-33.52-25.2-44.94-70.35-27.43-108.46a381.938 381.938 0 0 1 28.59-49.58 85.853 85.853 0 0 1 72.19-37.25c12.13 0.01 24.14 2.32 35.41 6.78l19.71 7.85c3.89 1.51 8.03 2.27 12.2 2.22 9.27-0.24 18.3-3.05 26.07-8.11 5.03-3.16 10.28-6.23 15.57-9.05a43.893 43.893 0 0 0 24.28-30.17L403.1 207c5.11-41.66 38.55-74.14 80.34-78.04 9.39-0.68 18.94-1.02 28.5-1.02s19.16 0.34 28.54 1.02c41.78 3.88 75.2 36.35 80.3 77.99l3.07 20.91a43.683 43.683 0 0 0 24.23 30.17c5.08 2.69 10.2 5.67 15.62 9.05 7.77 5.06 16.8 7.87 26.07 8.11 4.19 0.05 8.34-0.7 12.25-2.22l19.67-7.85a96.486 96.486 0 0 1 35.41-6.78 85.878 85.878 0 0 1 72.19 37.25 382.472 382.472 0 0 1 28.67 49.58c17.51 38.11 6.09 83.26-27.43 108.46L813.8 466.9a43.615 43.615 0 0 0-13.99 36.01v18.1a43.646 43.646 0 0 0 13.99 36.01l16.73 13.23c33.53 25.19 44.96 70.36 27.43 108.46a385.469 385.469 0 0 1-28.67 49.58 85.89 85.89 0 0 1-72.19 37.25 96.653 96.653 0 0 1-35.41-6.78l-19.67-7.85c-3.9-1.51-8.06-2.27-12.25-2.22-9.27 0.24-18.3 3.05-26.07 8.11-5.46 3.41-10.54 6.36-15.62 9.05a43.774 43.774 0 0 0-24.23 30.17l-3.07 20.95c-5.08 41.64-38.52 74.1-80.3 77.95-9.22 0.74-18.82 1.08-28.54 1.08zM333.89 684.63c21.28 0.32 42.07 6.49 60.07 17.83 4.82 2.94 8.53 5.12 12.12 7a106.302 106.302 0 0 1 57.3 77.4l3.03 20.91c0.9 11.89 9.89 21.59 21.67 23.38a335.229 335.229 0 0 0 47.7 0c11.79-1.79 20.78-11.49 21.67-23.38l3.03-20.91a106.376 106.376 0 0 1 57.34-77.4c3.88-2.09 7.68-4.27 12.16-7a115.66 115.66 0 0 1 60.03-17.83 95.768 95.768 0 0 1 35.67 6.78l19.67 7.85c3.71 1.52 7.68 2.32 11.69 2.35a23.19 23.19 0 0 0 19.41-9.39 319.137 319.137 0 0 0 23.85-41.3c4.45-11.06 0.53-23.72-9.39-30.34l-16.73-13.27a105.93 105.93 0 0 1-38.4-88.23v-14.16a106.066 106.066 0 0 1 38.4-88.28l16.73-13.23c9.92-6.63 13.84-19.31 9.39-30.38a319.137 319.137 0 0 0-23.85-41.3 23.24 23.24 0 0 0-19.46-9.43c-4 0.03-7.95 0.83-11.65 2.35l-19.67 7.89a95.936 95.936 0 0 1-35.71 6.78c-21.26-0.31-42.02-6.48-59.99-17.83-3.84-2.3-7.98-4.74-12.16-7.04a106.164 106.164 0 0 1-57.34-77.31l-3.03-20.91c-0.81-11.92-9.85-21.65-21.67-23.34a335.229 335.229 0 0 0-47.7 0c-11.83 1.69-20.87 11.42-21.67 23.34l-3.03 20.91a106.143 106.143 0 0 1-57.26 77.35c-3.63 1.92-7.47 4.27-12.12 7.04a115.66 115.66 0 0 1-60.03 17.83 96.18 96.18 0 0 1-35.67-6.83l-19.63-7.89c-3.7-1.51-7.65-2.31-11.65-2.35a23.226 23.226 0 0 0-19.46 9.43 319.405 319.405 0 0 0-23.89 41.3c-4.4 11.07-0.5 23.72 9.39 30.38l16.73 13.23c26.2 21.58 40.47 54.4 38.4 88.28v14.17c2.08 33.87-12.2 66.68-38.4 88.23l-16.73 13.27c-9.89 6.63-13.8 19.28-9.39 30.34a318.736 318.736 0 0 0 23.89 41.3 23.19 23.19 0 0 0 19.41 9.39c4.01-0.03 7.98-0.83 11.69-2.35l19.63-7.85c11.35-4.5 23.42-6.8 35.61-6.78z m178.05-12.8c-88.37 0-160-71.63-160-160s71.63-160 160-160 160 71.63 160 160v0.17c-0.09 88.33-71.67 159.91-160 160v-0.17z m0-256c-53.02 0-96 42.98-96 96s42.98 96 96 96c52.95 0 95.91-42.88 96-95.83-0.07-52.99-43.01-95.93-96-96v-0.17z" p-id="2557"></path></svg>
          }
        >
          <li className="px-4 py-2 font-hind capitalize text-sm">Setting</li>
        </Dropdown> */}
        <button
          type="button"
          // eslint-disable-next-line max-len
          className="transition ease duration-150 px-1 py-1 hover:shadow-md focus:shadow focus:outline-none"
          onClick={(e) => {
            e.preventDefault();
            onClose();
          }
          }
        >
          <svg t="1598243704614" className="w-5 h-5" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2152" width="200" height="200"><path d="M682.98 728.11L512.32 557.44 341.43 728.11c-12.5 12.56-32.82 12.61-45.38 0.11s-12.61-32.82-0.11-45.38l170.88-170.67-170.86-171.09c-12.5-12.5-12.5-32.77 0-45.27s32.77-12.5 45.27 0L512.1 466.69l170.67-170.88c12.5-12.5 32.77-12.5 45.27 0s12.5 32.77 0 45.27L557.38 511.96l170.67 170.67c12.5 12.5 12.5 32.77 0 45.27s-32.77 12.5-45.27 0l0.2 0.21z" p-id="2153"></path></svg>
        </button>
      </div>
    </div>
  );
}

HeadMessaging.propTypes = {
  onClose: PropTypes.func,
  currentChannel: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  onMinimaze: PropTypes.func,
  userIdStore: PropTypes.string,
};

/**
 *
 * @param {{
 *  messageList: array,
 *  className: string,
 *  userId: string,
 *  customData: object
 *  }} props
 * @return { function }
 */
function Body(props) {
  const { messageList, className, userId, customData, setCustomData } = props;
  // eslint-disable-next-line max-len
  const classNameContainer = classnames('transition-all ease duration-150 flex flex-col-reverse overflow-y-auto px-2 pb-2 pt-4 w-full beautymates__scrooll', className);

  return (
    <div
      className={classNameContainer}
      style={{ maxHeight: 300, height: 300, maxWidth: 300, width: 300 }}
    >
      <div className="flex flex-col" style={{ maxWidth: '100%' }}>
        {_.map(messageList, function (messageItem, index) {
          const prev = messageList[index - 1];
          const prevCreatedAt = prev && prev.createdAt;
          const currentCreatedAt = messageItem.createdAt;
          // eslint-disable-next-line max-len
          const hasSeparator = !(prevCreatedAt && moment(currentCreatedAt).isSame(moment(prevCreatedAt), 'day'));
          const isUser = _.get(messageItem, '_sender.userId') === userId;

          return (
            <React.Fragment key={messageItem.messageId}>
              {hasSeparator && (
                <div className="w-full flex items-center justify-center">
                  <span className="beautymates__divider_text">
                    {moment(currentCreatedAt).format('DD MMMM YYYY')}
                  </span>
                </div>
              )}
              {messageItem.messageType === 'user' && (
                <MessageItem
                  message={messageItem.message}
                  messageId={messageItem.messageId}
                  messageTime={messageItem.createdAt}
                  messageData={messageItem.data}
                  customType={messageItem.customType}
                  isUser={isUser}
                />
              )}
              {messageItem.messageType === 'file' && (
                <MessageImage
                  src={_.get(messageItem, 'thumbnails[1].url')}
                  alt={_.get(messageItem, 'name')}
                  messageTime={_.get(messageItem, 'createdAt')}
                  isUser={isUser}
                />
              )}
            </React.Fragment>
          );
        })}
        <Preview customData={customData} setCustomData={setCustomData} />
      </div>
    </div>
  );
}

Body.propTypes = {
  messageList: PropTypes.array,
  className: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  userId: PropTypes.string,
  customData: PropTypes.object,
};

/**
 *
 * @param {{
 *  className: string,
 *  currentChannel: string,
 *  setMessageList: function,
 *  customData: object,
 *  setCustomData: function,
 * }} props
 * @return {function}
 */
function Input(props) {
  const {
    className,
    currentChannel,
    setMessageList,
    customData,
    setCustomData,
  } = props;

  const [errors, setErrors] = React.useState(false);
  const [isLoadSend, setIsLoad] = React.useState(false);
  const [visible, setVisible] = React.useState(false);

  const { register, handleSubmit, reset } = useForm();

  const refContainers = React.useRef();
  const refInputFile = React.useRef();

  React.useEffect(() => {
    window.addEventListener('mousedown', handleClose);
    return window.addEventListener('mousedown', handleClose);
  });

  React.useEffect(() => {
    if (errors !== false) {
      console.error(errors);
    }
  }, [errors]);

  /**
   *
   * @param {node} event
   */
  function handleClose(event) {
    // eslint-disable-next-line max-len
    if (refContainers.current && !refContainers.current.contains(event.target)) {
      setVisible(false);
    }
  }

  /**
   * Set File
   */
  function handleClick() {
    refInputFile.current.click();
  }

  /**
   *
   * @param {{File}} file - get a file on input
   */
  function sendFile(file) {
    // eslint-disable-next-line new-cap
    const sb = new Sendbird.getInstance();
    const paramsFile = new sb.FileMessageParams();
    paramsFile.file = file;
    paramsFile.fileName = file.name;
    paramsFile.fileSize = file.size;

    // eslint-disable-next-line max-len
    paramsFile.thumbnailSizes = [{ maxWidth: 100, maxHeight: 100 }, { maxWidth: 200, maxHeight: 200 }];
    paramsFile.mentionType = 'users'; // Either 'users' or 'channel'
    // eslint-disable-next-line max-len
    paramsFile.pushNotificationDeliveryOption = 'default'; // Either 'default' or 'suppress'

    // eslint-disable-next-line max-len
    currentChannel.sendFileMessage(paramsFile, function (fileMessage, errorSendFile) {
      if (errorSendFile) {
        return setErrors({ message: 'Sending File Failed', error: errorSendFile });
      }

      setMessageList((prev) => [...prev, fileMessage]);
      setIsLoad(false);
    });
  }

  /**
   *
   * @param {string} text - text from input
   */
  function sendText(text) {
    const sb = new Sendbird.getInstance();
    const params = new sb.UserMessageParams();

    if (customData) {
      const convertData = JSON.stringify(customData);
      params.customType = customData.typeData;
      params.data = convertData;
    }

    params.message = text;
    currentChannel.sendUserMessage(params, function (message, errorSendMessage) {
      if (errorSendMessage) {
        setIsLoad(false);
        return setErrors({ message: 'Sending Failed', error: errorSendMessage });
      }
      setMessageList((prev) => [...prev, message]);
      setIsLoad(false);
      return reset();
    });
  }

  /**
   *
   * @param {object} e - a object get in form
   */
  function onSubmit(e) {
    setIsLoad(true);
    const isEmptyFile = _.size(e.messageFileImage) === 0;

    if (!isEmptyFile) {
      sendFile(e.messageFileImage[0]);
    }

    if (e.messageText) {
      sendText(e.messageText);
      setCustomData(null);
    }
  }

  // eslint-disable-next-line max-len
  const classNameContainer = classnames('transition-all ease duration-150 w-full py-2 px-2 border-t border-gray-300', className);

  // eslint-disable-next-line max-len
  const classNameInput = classnames('absolute flex items-center bg-white rounded shadow-lg bottom-0 right-0 mb-12 mr-2 transition-all duration-150 transform', {
    'scale-100 ease-in block': visible,
    'scale-0 ease-out hidden': !visible,
  });

  return (
    <div className={classNameContainer}>
      <form
        className="w-full flex items-center"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="w-full flex items-center relative">
          <input
            name="messageText"
            placeholder="Write something"
            // eslint-disable-next-line max-len
            className="h-10 py-2 px-3 text-sm font-hind w-full rounded-full border border-gray-300 focus:outline-none"
            ref={register()}
          />
          <input
            name="messageFileImage"
            type="file"
            accept="image/x-png,image/gif,image/jpeg"
            className="hidden"
            ref={(el) => {
              refInputFile.current = el;
              register(el);
            }}
          />
          <div
            className="absolute right-0 bottom-0 top-0 mr-1"
            style={{ marginTop: 5 }}
          >
            <div className="relative" ref={refContainers}>
              <button
                type="button"
                // eslint-disable-next-line max-len
                className="rounded-full focus:outline-none text-bm p-2 bg-bm-900 text-bm .mr-1"
                onClick={() => setVisible((prev) => !prev)}>
                <svg t="1597333153584" className="icon w-3 h-3 fill-current text-white" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4336" width="200" height="200"><path d="M511 68c16.506 0 29.887 13.38 29.887 29.887l-0.001 386.226h386.227C943.62 484.113 957 497.494 957 514s-13.38 29.887-29.887 29.887l-386.227-0.001v386.227C540.887 946.62 527.507 960 511 960s-29.887-13.38-29.887-29.887V543.886H94.887C78.38 543.887 65 530.507 65 514s13.38-29.887 29.887-29.887h386.226V97.887C481.113 81.38 494.494 68 511 68z" fill="#ffffff" p-id="4337"></path></svg>
              </button>
              <div className={classNameInput}>
                {/* <button className="px-2 py-2">
                  <svg t="1597332003868" className="icon w-8 h-8" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4200" width="200" height="200"><path d="M682.47 101c72.01 0 130.041 59.663 131.194 133.157l0.018 2.231v467.066c0 16.519-13.383 29.91-29.891 29.91-16.273 0-29.509-13.012-29.883-29.204l-0.009-0.706V236.388c0-41.574-31.608-74.895-70.255-75.557l-1.174-0.01H202.212c-38.787 0-70.784 32.905-71.42 74.31l-0.01 1.257v601.224c0 41.574 31.609 74.895 70.256 75.557l1.174 0.01h642.917c21.34 0 38.686-17.111 39.082-38.37l0.007-0.743V199.292c0-16.52 13.382-29.91 29.89-29.91 16.273 0 29.509 13.01 29.892 29.204v675.48c0 54.093-43.385 98.047-97.236 98.92l-1.635 0.014H202.212c-72.01 0-130.041-59.663-131.195-133.157L71 837.612V236.388c0-73.768 57.311-134.17 129.034-135.37l2.178-0.018H682.47z m-78.027 616.612c12.7 0 22.994 10.301 22.994 23.008 0 12.492-9.949 22.658-22.35 23l-0.644 0.008H289.436c-12.699 0-22.993-10.301-22.993-23.008 0-12.491 9.948-22.658 22.35-22.999l0.643-0.009h315.007z m0-138.047c12.7 0 22.994 10.3 22.994 23.008 0 12.491-9.949 22.658-22.35 22.999l-0.644 0.008H289.436c-12.699 0-22.993-10.3-22.993-23.007 0-12.492 9.948-22.658 22.35-23l0.643-0.008h315.007z m0-138.048c12.7 0 22.994 10.301 22.994 23.008 0 12.492-9.949 22.658-22.35 23l-0.644 0.008H289.436c-12.699 0-22.993-10.301-22.993-23.008 0-12.491 9.948-22.658 22.35-22.999l0.643-0.009h315.007z m-75.876-132.181c12.7 0 22.994 10.3 22.994 23.008 0 12.491-9.949 22.658-22.35 22.999l-0.644 0.009H366.343c-12.699 0-22.993-10.301-22.993-23.008 0-12.492 9.948-22.658 22.35-23l0.643-0.008h162.224z" fill="#333333" p-id="4201"></path></svg>
                </button>
                <button className="px-2 py-2">
                  <svg t="1597331687826" className="icon w-8 h-8" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4064" width="200" height="200"><path d="M503.297 73.86a29.927 29.927 0 0 1 29.406 0l360.089 203.131A29.89 29.89 0 0 1 908 303.024v416.952a29.89 29.89 0 0 1-15.208 26.033l-360.089 203.13a29.927 29.927 0 0 1-29.406 0L143.208 746.01A29.89 29.89 0 0 1 128 719.976V303.024a29.89 29.89 0 0 1 15.208-26.033zM518 134.219L187.823 320.475v382.047L518 888.778 848.177 702.52V320.475L518 134.218zM762.82 335.63c14.286-7.588 32.099-2.435 40.085 11.731l0.36 0.656c7.592 14.278 2.436 32.08-11.739 40.061l-240.168 135.22v280.236l-0.009 0.705c-0.375 16.184-13.62 29.188-29.903 29.188-0.384 0-0.767-0.007-1.147-0.022-0.381 0.015-0.764 0.022-1.149 0.022-16.283 0-29.528-13.004-29.903-29.188l-0.008-0.705V523.299L249.07 388.079c-14.174-7.981-19.33-25.784-11.738-40.062l0.36-0.656c7.985-14.166 25.799-19.319 40.085-11.73l0.657 0.358 241.864 136.177 241.865-136.177z" fill="#333333" p-id="4065"></path></svg>
                </button> */}
                <button
                  type="button"
                  className="px-2 py-2 focus:outline-none"
                  onClick={() => handleClick()}
                >
                  <svg t="1597331143764" className="icon w-8 h-8" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3928" width="200" height="200"><path d="M717.344 892.2c16.52 0 29.914 13.387 29.914 29.9 0 16.277-13.013 29.517-29.208 29.892l-0.706 0.008H307.557c-16.52 0-29.913-13.387-29.913-29.9 0-16.277 13.012-29.517 29.207-29.892l0.706-0.008h409.787zM724.045 78C791.399 78 846 132.576 846 199.9v517.5c0 67.324-54.601 121.9-121.955 121.9h-421.09C235.601 839.3 181 784.724 181 717.4V199.9C181 132.576 235.601 78 302.955 78z m-333.06 427.254l-0.638 0.53-143.921 122.62a30.009 30.009 0 0 1-5.6 3.777l0.001 85.219c0 34.297 27.816 62.1 62.128 62.1h421.09a62.77 62.77 0 0 0 7.759-0.48L436.127 506.67c-12.636-11.64-31.87-12.192-45.143-1.416z m258.684-113.839l-0.61 0.58L527.9 509.882l252.859 232.91a61.818 61.818 0 0 0 5.406-24.366l0.008-1.027V479.357a29.975 29.975 0 0 1-4.408-3.442l-0.564-0.546-83.744-83.133c-13.165-13.068-34.26-13.368-47.788-0.821zM724.045 137.8h-421.09c-34.312 0-62.128 27.803-62.128 62.1l-0.001 354.698 110.711-94.325c35.957-30.634 88.965-29.922 124.074 1.46l1.059 0.961 7.171 6.606 123.487-120.156c36.521-35.536 94.584-35.602 131.185-0.415l1.103 1.077 46.557 46.218V199.9c0-33.954-27.262-61.543-61.1-62.092l-1.028-0.008z m-319.844 48.3c53.372 0 96.643 43.252 96.643 96.6 0 53.348-43.271 96.6-96.643 96.6-53.373 0-96.644-43.252-96.644-96.6 0-53.348 43.271-96.6 96.644-96.6z m0 46c-27.956 0-50.623 22.657-50.623 50.6s22.667 50.6 50.623 50.6c27.955 0 50.623-22.657 50.623-50.6s-22.668-50.6-50.623-50.6z" fill="#333333" p-id="3929"></path></svg>
                </button>
              </div>
            </div>
          </div>
        </div>
        <button
          type="submit"
          className="bg-bm-900 px-2 py-2 rounded-full ml-2 focus:outline-none"
        >
          {isLoadSend ? (
            <svg
              viewBox="0 0 1024 1024"
              focusable="false"
              className="animate-spin h-5 w-5 text-white"
              data-icon="loading"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
            >
              <path d="M988 548c-19.9 0-36-16.1-36-36 0-59.4-11.6-117-34.6-171.3a440.45 440.45 0 0 0-94.3-139.9 437.71 437.71 0 0 0-139.9-94.3C629 83.6 571.4 72 512 72c-19.9 0-36-16.1-36-36s16.1-36 36-36c69.1 0 136.2 13.5 199.3 40.3C772.3 66 827 103 874 150c47 47 83.9 101.8 109.7 162.7 26.7 63.1 40.2 130.2 40.2 199.3.1 19.9-16 36-35.9 36z"></path>
            </svg>
          ) : (
              <svg t="1596950655072" className="icon w-5 h-5 fill-current text-white" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3773" width="200" height="200"><path d="M85.333333 896l896-384L85.333333 128v298.666667l640 85.333333-640 85.333333v298.666667z" fill="" p-id="3774"></path></svg>
            )}
        </button>
      </form>
    </div>
  );
}

Input.propTypes = {
  className: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  currentChannel: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  setMessageList: PropTypes.func,
  customData: PropTypes.object,
  setCustomData: PropTypes.func,
};

/**
 * Pop Up Modal Chatting
 * @param {{ visible: boolend, onClose: function, appId: string, userId: string, customData: object, userIdStore: string, urlChannel: string }} props
 * @return {function}
 */
function Messaging(props) {
  const { visible, onClose, appId, userId, customData, userIdStore, urlChannel } = props;
  const sb = new Sendbird({ appId });
  // eslint-disable-next-line no-unused-vars
  const [isLoadListMessage, setIsLoadListMessage] = React.useState(false);

  const [errors, setErrors] = React.useState(false);
  const [isVisible, setIsVisible] = React.useState(visible);
  const [minimaze, setMiniMaze] = React.useState(false);

  const [objectChannel, setObjectChannel] = React.useState(null);
  const [messageList, setMessageList] = React.useState([]);

  const [stateCustomData, setStateCustomData] = React.useState(null);

  React.useEffect(() => {
    if (visible) {
      sb.connect(userId, function (userinfo, errorConnect){
        if (errorConnect) {
          setErrors({ message: 'Connection Failed', error: errorConnect })
        }
      });
    }
  }, [visible]);

  React.useEffect(() => {
    if (urlChannel !== null && visible) {
      sb.GroupChannel.getChannel(urlChannel, function (groupChannel, errorWhileGetChannel) {
        if (errorWhileGetChannel) {
          setErrors({ message: 'Error Get Channel', error: errorWhileGetChannel });

        }

        setObjectChannel(groupChannel);
        setStateCustomData(customData);
      });
    }
  }, [visible, urlChannel]);

  React.useEffect(() => {
    setStateCustomData(customData);
    setIsVisible(visible);
  }, [visible]);

  React.useEffect(() => {
    if (objectChannel !== null) {
      getMessageList(objectChannel);
    }
  }, [objectChannel]);

  const defaultHandlers = {
    onMessageReceived: (channel, message) => {
      if (objectChannel !== null) {
        objectChannel.refresh(function(response, errorRefresh){
          if (errorRefresh) {
            return;
          }
  
          setObjectChannel(response);
        });
      }
      setMessageList(prev => [...prev, message]);
    },
    onTypingStatusUpdated: (groupChannel) => {
      const members = groupChannel.getTypingMembers();
    }
  }

  const arrayHandlers = [
    'onMessageReceived',
    'onMessageUpdated',
    'onMessageDeleted',
    'onMentionReceived',
    'onChannelChanged',
    'onChannelDeleted',
    'onChannelFrozen',
    'onChannelUnfrozen',
    'onMetaDataCreated',
    'onMetaDataUpdated',
    'onMetaDataDeleted',
    'onMetaCountersCreated',
    'onMetaCountersUpdated',
    'onMetaCountersDeleted',
    'onChannelHidden',
    'onUserReceivedInvitation',
    'onUserDeclinedInvitation',
    'onUserJoined',
    'onUserLeft',
    'onDeliveryReceiptUpdated',
    'onReadReceiptUpdated',
    'onTypingStatusUpdated',
    'onUserEntered',
    'onUserExited',
    'onUserMuted',
    'onUserUnmuted',
    'onUserBanned',
    'onUserUnbanned',
  ];

  const keyProps = _.keys(props);
  const keyDefaultHandlers = _.keys(defaultHandlers);

  React.useEffect(() => {
    const objHandlers = new sb.ChannelHandler();

    arrayHandlers.map(event => {
      if (keyProps.includes(event)) {
        if (keyDefaultHandlers.includes(event)) {
          objHandlers[event] = (...params) => {
            defaultHandlers[event](...params)
            props[event](...params)
          }
        } else {
          objHandlers[event] = props[event]
        }
      } else {
        keyDefaultHandlers.includes(event) ?
          objHandlers[event] = defaultHandlers[event]
          :
          objHandlers[event] = () => { }
      }
    })

    // channelHandler.onMessageReceived = function(channel, message) {
    //   setMessageList((prev) => [...prev, message]);
    // };

    sb.addChannelHandler('handler_channel_modal_pop_up', objHandlers);
    return () => sb.removeChannelHandler('handler_channel_modal_pop_up');
  });

  // React.useEffect(() => {
  //   const connectionHandlers = new sb.ConnectionHandler();
  //   connectionHandlers.onReconnectStarted = function () {
  //     console.log('Start Connect');
  //   }

  //   connectionHandlers.onReconnectSucceeded = function () {
  //     console.log('Success Connect');
  //   }

  //   connectionHandlers.onReconnectFailed = function () {
  //     console.log('Failed Connect');
  //   }

  //   sb.addConnectionHandler('handlers_connection_list_page', connectionHandlers);
  //   return () => sb.removeConnectionHandler('handlers_connection_list_page');
  // });

  /**
   *
   * @param {string} currentChannel - channel url
   */
  function getMessageList(currentChannel) {
    const getPrevListMessage = currentChannel.createPreviousMessageListQuery();
    getPrevListMessage.limit = 8;
    getPrevListMessage.reverse = false;
    getPrevListMessage.includeMetaArray = true;
    getPrevListMessage.includeReaction = false;
    setIsLoadListMessage(true);
    getPrevListMessage.load((messages, errorListMessage) => {
      if (errorListMessage) {
        return setErrors({ message: 'Error Load Message', error: errorListMessage });
      }

      setMessageList(messages);
      return setIsLoadListMessage(false);
    });
  }

  /**
   * function for onClose Modal Pop Up
   */
  function close() {
    setIsVisible(false);
    if (onClose) {
      onClose();
    }
  }

  /**
   * function for Minimize
   */
  function onMinimaze() {
    setMiniMaze((prev) => !prev);
  }

  if (!visible) {
    return <div id="message-container"></div>;
  }

  const classNameContainer = classnames('fixed transition-all duration-150 bottom-0 right-0 bg-white mx-6 border border-gray-300 rounded-t-lg', {
    'h-zero ease-out': !isVisible,
    'h-416 ease-in': isVisible,
  });

  const classNameBody = minimaze ? 'transform h-0 hidden' : 'block';
  const classNameInput = minimaze ? 'transform h-0 hidden' : 'block';

  return (
    <div className={classNameContainer} style={{ width: 300, maxWidth: 300 }}>
      <HeadMessaging
        currentChannel={objectChannel}
        onClose={close}
        onMinimaze={onMinimaze}
        userId={userId}
        userIdStore={userIdStore}
      />
      <Body
        className={classNameBody}
        messageList={messageList}
        errors={errors}
        userId={userId}
        customData={stateCustomData}
        setCustomData={setStateCustomData}
      />
      <Input
        className={classNameInput}
        currentChannel={objectChannel}
        setMessageList={setMessageList}
        customData={stateCustomData}
        setCustomData={setStateCustomData}
      />
    </div>
  );
}

Messaging.propTypes = {
  channel: PropTypes.object,
  urlChannel: PropTypes.string.isRequired,
  visible: PropTypes.bool,
  customData: PropTypes.object,
  onClose: PropTypes.func,
  appId: PropTypes.string,
  userId: PropTypes.string,
  userIdStore: PropTypes.string,
    /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageReceived: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageUpdated: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageDeleted: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMentionReceived: PropTypes.func,
  onChannelChanged: PropTypes.func,
  onChannelDeleted: PropTypes.func,
  onChannelFrozen: PropTypes.func,
  onChannelUnfrozen: PropTypes.func,
  onMetaDataCreated: PropTypes.func,
  onMetaDataUpdated: PropTypes.func,
  onMetaDataDeleted: PropTypes.func,
  onMetaCountersCreated: PropTypes.func,
  onMetaCountersUpdated: PropTypes.func,
  onMetaCountersDeleted: PropTypes.func,
  onChannelHidden: PropTypes.func,
  onUserReceivedInvitation: PropTypes.func,
  onUserDeclinedInvitation: PropTypes.func,
  onUserJoined: PropTypes.func,
  onUserLeft: PropTypes.func,
  onDeliveryReceiptUpdated: PropTypes.func,
  onReadReceiptUpdated: PropTypes.func,
  onTypingStatusUpdated: PropTypes.func,
  onUserEntered: PropTypes.func,
  onUserExited: PropTypes.func,
  onUserMuted: PropTypes.func,
  onUserUnmuted: PropTypes.func,
  onUserBanned: PropTypes.func,
  onUserUnbanned: PropTypes.func,
};

Messaging.defaultProps = {
  visible: false,
};

export default Messaging;
