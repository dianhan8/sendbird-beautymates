export const rupiah = (angka, withPrefix = true) => {
  const reverse = angka
    .toString()
    .split('')
    .reverse()
    .join('');
  let ribuan = reverse.match(/\d{1,3}/g);
  ribuan = ribuan
    .join('.')
    .split('')
    .reverse()
    .join('');

  if (angka.toString().indexOf('-') >= 0) {
    ribuan = `-${ribuan}`;
  }

  if (withPrefix) {
    ribuan = `Rp ${ribuan}`;
  }
  return ribuan;
};

/**
 * Convert file to base64 string
 * @param {object} file
 */
export const convertFileToBase64 = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
