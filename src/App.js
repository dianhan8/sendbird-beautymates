import React, { memo } from 'react';
import PropTypes, { object } from 'prop-types';
import classnames from 'classnames';
import Sendbird from 'sendbird';
import _, { snakeCase } from 'lodash';
import 'lazysizes';
import './styles.css';
import './app.css';

import Img from './components/Img';
import ChannelList from './components/ChannelList';
import MessageList from './components/MessageList';

import Messaging from './Messaging';

export const MessagingPopUp = Messaging;

/**
 *
 * @param {{ appId, userId, limitMessage  }} props - AppId is sendbird
 * @return {function}
 */
function App(props) {
  const {
    appId,
    userId,
    limitMessage,
    textHome,
  } = props
  const [isLoadListChannel, setIsLoadListChannel] = React.useState(true);
  const [isLoadListMessage, setIsLoadListMessage] = React.useState(false);

  const [messageList, setMessageList] = React.useState([]);
  const [currentChannel, setCurrentChannel] = React.useState(false);
  const [channelList, setChannelList] = React.useState([]);
  const [isTyping, setIsTyping] = React.useState(false);
  const [userInfo, setUserInfo] = React.useState(false);
  const [errors, setErrors] = React.useState(false);

  const sb = new Sendbird({ appId });

  const arrayHandlers = [
    'onMessageReceived',
    'onMessageUpdated',
    'onMessageDeleted',
    'onMentionReceived',
    'onChannelChanged',
    'onChannelDeleted',
    'onChannelFrozen',
    'onChannelUnfrozen',
    'onMetaDataCreated',
    'onMetaDataUpdated',
    'onMetaDataDeleted',
    'onMetaCountersCreated',
    'onMetaCountersUpdated',
    'onMetaCountersDeleted',
    'onChannelHidden',
    'onUserReceivedInvitation',
    'onUserDeclinedInvitation',
    'onUserJoined',
    'onUserLeft',
    'onDeliveryReceiptUpdated',
    'onReadReceiptUpdated',
    'onTypingStatusUpdated',
    'onUserEntered',
    'onUserExited',
    'onUserMuted',
    'onUserUnmuted',
    'onUserBanned',
    'onUserUnbanned',
  ];

  const defaultHandlers = {
    onMessageReceived: (channel, message) => {
      document.title = 'A new message from ' + message.sender.nickname;
      getChannelList();
      if (channel.url === currentChannel.url) {
        setMessageList((prev) => [...prev, message]);
      }
    },
    onTypingStatusUpdated: function (groupChannel) {
      const members = groupChannel.getTypingMembers();
      if (members.length !== 0) {
        setIsTyping(true);
      } else {
        setIsTyping(false);
      }
    }
  }

  const keyProps = Object.keys(props);
  const keyDefaultHandlers = Object.keys(defaultHandlers);

  React.useEffect(() => {
    const objectHandlers = new sb.ChannelHandler();

    arrayHandlers.map(event => {
      if (keyProps.includes(event)) {
        if (keyDefaultHandlers.includes(event)) {

          objectHandlers[event] = (...params) => {
            defaultHandlers[event](...params);
            props[event](...params);
          }
        } else {
          objectHandlers[event] = (...params) => props[event](...params);
        }
      } else {
        keyDefaultHandlers.includes(event) ? 
          objectHandlers[event] = (...params) => {
            defaultHandlers[event](...params)
          }
        :
          objectHandlers[event] = () => {}
      }
    })

    sb.addChannelHandler('handlers_chat_list_page', objectHandlers);
    return () => sb.removeChannelHandler('handlers_chat_list_page');
  });

  React.useEffect(() => {
    const connectionHandlers = new sb.ConnectionHandler();
    connectionHandlers.onReconnectStarted = function () {
      console.log('Start Connect');
    }

    connectionHandlers.onReconnectSucceeded = function () {
      console.log('Success Connect');
    }

    connectionHandlers.onReconnectFailed = function () {
      console.log('Failed Connect');
    }

    sb.addConnectionHandler('handlers_connection_list_page', connectionHandlers);
    return () => sb.removeConnectionHandler('handlers_connection_list_page');
  });

  React.useEffect(() => {
    sb.connect(userId, function (userInformation, errorConnect) {
      if (errorConnect) {
        return setErrors({ message: 'error from connect', error: errorConnect });
      }

      setUserInfo(userInformation);
      return getChannelList();
    });
  }, []);

  React.useEffect(() => {
    if (currentChannel !== false) {
      getMessageList();
    }
  }, [currentChannel]);

  /**
   * Get Channel
   */
  function getChannelList() {
    const getWithUserId = sb.GroupChannel.createMyGroupChannelListQuery();
    getWithUserId.userIdsExactFilter = userId;
    setIsLoadListChannel(true);
    getWithUserId.next((groupChannel, error) => {
      if (error) {
        return setErrors({ message: 'error get channel', error });
      }

      setChannelList(groupChannel);
      return setIsLoadListChannel(false);
    });
  }

  /**
   * Get MessageList
   */
  function getMessageList() {
    const getPrevListMessage = currentChannel.createPreviousMessageListQuery();
    getPrevListMessage.limit = limitMessage;
    getPrevListMessage.reverse = false;
    getPrevListMessage.includeMetaArray = true;
    getPrevListMessage.includeReaction = false;
    setIsLoadListMessage(true);
    getPrevListMessage.load((messages, errorListMessage) => {
      if (errorListMessage) {
        return setErrors({
          message: 'error load message', error: errorListMessage,
        });
      }

      setMessageList(messages);
      return setIsLoadListMessage(false);
    });
  }

  /**
   *
   * @param {string} text
   */
  function search(text) {
    setIsLoadListChannel(true);
    const searchChannnelQuery = sb.GroupChannel.createMyGroupChannelListQuery();
    searchChannnelQuery.includeEmpty = true;
    searchChannnelQuery.nicknameContainsFilter = text;

    searchChannnelQuery.next(function (groupChannels, errorGetSearch) {
      if (errorGetSearch) {
        return;
      }

      setChannelList(groupChannels);
      return setIsLoadListChannel(false);
    });
  }

  // eslint-disable-next-line max-len
  const classContainer = classnames('w-full h-auto border border-gray-100 flex shadow-lg rounded-lg');
  const classMessageContainer = classnames('w-full py-2 px-2');

  // if channel not selected
  // eslint-disable-next-line max-len
  const classWelcomeText = classnames('w-full flex flex-col items-center justify-center', {
    'hidden': currentChannel,
    'block': !currentChannel,
  });
  const classMessageList = classnames('flex flex-col', {
    'hidden': !currentChannel,
    'block': currentChannel,
  });

  return (
    <div className={classContainer}>
      <div
        style={{ width: 360 }}
        className="py-4 rounded-tl-lg rounded-bl-lg bg-white"
      >
        <div className="py-5 px-4 flex items-center justify-center">
          <span className="font-poppins text-2xl">beautymates</span>
        </div>
        <div className="w-full py-3 px-4">
          <div className="flex items-center relative">
            <input
              type="text"
              placeholder="Search.."
              // eslint-disable-next-line max-len
              className="rounded-full w-full bg-white border border-gray-300 py-1 text-sm font-hind px-3 focus:outline-none"
              onChange={(e) => search(e.target.value)}
            />
            <svg t="1601438814928" className="w-5 h-5 absolute top-0 bottom-0 right-0 mr-3 my-auto fill-current text-gray-500" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3382" width="200" height="200"><path d="M949.3504 794.0096l-227.328-227.371886a356.176457 356.176457 0 0 0 37.668571-159.700114c0-197.617371-160.768-358.4-358.4-358.4s-358.4 160.782629-358.4 358.4 160.768 358.4 358.4 358.4c60.854857 0 118.184229-15.286857 168.433372-42.144914l224.548571 222.500571c20.465371 20.48 47.733029 28.335543 76.770743 28.335543h0.555886c29.125486 0 56.554057-8.192 77.2096-28.8768 20.670171-20.655543 32.138971-45.041371 32.285257-74.181486a106.920229 106.920229 0 0 0-31.744-76.960914z m-248.4224-22.367086l71.8848-71.8848 23.215543 23.200915-71.767772 71.767771-23.332571-23.083886zM101.419886 406.9376c0-165.639314 134.261029-299.885714 299.885714-299.885714s299.885714 134.2464 299.885714 299.885714c0 165.653943-134.261029 299.885714-299.885714 299.885714s-299.885714-134.231771-299.885714-299.885714z m589.385143 210.929371l61.308342 61.2352-71.9872 71.972572-60.693942-60.035657a361.515886 361.515886 0 0 0 71.3728-73.172115z m216.634514 285.930058c-9.698743 9.684114-22.528 11.702857-36.1472 11.702857h-0.263314c-13.399771 0-25.965714-1.930971-35.401143-11.351772l-90.580115-89.219657 71.665372-71.504457 91.253028 91.2384c9.479314 9.479314 14.672457 21.123657 14.599315 34.640457-0.043886 13.604571-5.4272 24.7808-15.125943 34.494172z" fill="" p-id="3383"></path><path d="M369.722514 171.329829c-5.705143 0-11.3664 0.234057-16.969143 0.687542a14.628571 14.628571 0 1 0 2.399086 29.169372c4.798171-0.394971 9.669486-0.599771 14.570057-0.599772a14.628571 14.628571 0 1 0 0-29.257142zM284.715886 189.8496a204.726857 204.726857 0 0 0-119.018057 185.534171 14.628571 14.628571 0 1 0 29.257142 0 175.396571 175.396571 0 0 1 101.961143-158.954057 14.628571 14.628571 0 1 0-12.200228-26.580114z" fill="" p-id="3384"></path></svg>
          </div>
        </div>
        <ChannelList
          userId={userId}
          isLoad={isLoadListChannel}
          channelList={channelList}
          setCurrentChannel={setCurrentChannel}
        />
      </div>
      <div
        className={classMessageContainer}
        style={{ backgroundColor: '#F5D3D4' }}
      >
        <div
          className={classWelcomeText}
          style={{ minHeight: 680, maxHeight: 680 }}
        >
          {textHome}
        </div>
        <div className={classMessageList}>
          <MessageList
            currentChannel={currentChannel}
            messageList={messageList}
            setMessageList={setMessageList}
            userId={userId}
            isTyping={isTyping}
          />
        </div>
      </div>
    </div>
  );
}

App.propTypes = {
  /**
   * This appId generete by Sendbird
   */
  appId: PropTypes.string.isRequired,
  /**
   * userId generate by Sendbird
   */
  userId: PropTypes.string.isRequired,
  /**
   * Setting message list
   */
  limitMessage: PropTypes.number,
  /**
   * You can edit messaage if channel not selected, use node.
   */
  textHome: PropTypes.node,
    /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageReceived: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageUpdated: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageDeleted: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMentionReceived: PropTypes.func,
  onChannelChanged: PropTypes.func,
  onChannelDeleted: PropTypes.func,
  onChannelFrozen: PropTypes.func,
  onChannelUnfrozen: PropTypes.func,
  onMetaDataCreated: PropTypes.func,
  onMetaDataUpdated: PropTypes.func,
  onMetaDataDeleted: PropTypes.func,
  onMetaCountersCreated: PropTypes.func,
  onMetaCountersUpdated: PropTypes.func,
  onMetaCountersDeleted: PropTypes.func,
  onChannelHidden: PropTypes.func,
  onUserReceivedInvitation: PropTypes.func,
  onUserDeclinedInvitation: PropTypes.func,
  onUserJoined: PropTypes.func,
  onUserLeft: PropTypes.func,
  onDeliveryReceiptUpdated: PropTypes.func,
  onReadReceiptUpdated: PropTypes.func,
  onTypingStatusUpdated: PropTypes.func,
  onUserEntered: PropTypes.func,
  onUserExited: PropTypes.func,
  onUserMuted: PropTypes.func,
  onUserUnmuted: PropTypes.func,
  onUserBanned: PropTypes.func,
  onUserUnbanned: PropTypes.func,
};

App.defaultProps = {
  limitMessage: 30,
  textHome: (
    <div className="flex flex-col items-center">
      <span className="font-hind font-semibold text-lg text-white">
        Welcome to beautymates
      </span>
      <p className="font-hind text-sm">
        Order some product, and message some store.
      </p>
    </div>
  ),
};

export default App;