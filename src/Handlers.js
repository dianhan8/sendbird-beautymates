/* eslint-disable array-callback-return */
import React from 'react';
import PropTypes from 'prop-types';
import SendBird from 'sendbird';

const GroupChannel = [
  'onMessageReceived',
  'onMessageUpdated',
  'onMessageDeleted',
  'onMentionReceived',
  'onChannelChanged',
  'onChannelDeleted',
  'onChannelFrozen',
  'onChannelUnfrozen',
  'onMetaDataCreated',
  'onMetaDataUpdated',
  'onMetaDataDeleted',
  'onMetaCountersCreated',
  'onMetaCountersUpdated',
  'onMetaCountersDeleted',
  'onChannelHidden',
  'onUserReceivedInvitation',
  'onUserDeclinedInvitation',
  'onUserJoined',
  'onUserLeft',
  'onDeliveryReceiptUpdated',
  'onReadReceiptUpdated',
  'onTypingStatusUpdated',
  'onUserEntered',
  'onUserExited',
  'onUserMuted',
  'onUserUnmuted',
  'onUserBanned',
  'onUserUnbanned',
];

const UsersHandlers = [
  'onFriendsDiscovered',
  'onTotalUnreadMessageCountUpdated',
];

const ConnectionHandlers = [
  'onReconnectStarted',
  'onReconnectSucceeded',
  'onReconnectFailed ',
];

/**
 * Use Channel Handlers
 * @param {String} appId - Generate by Sendbird
 * @param {String} userId - Generate by Sendbird
 * @param {{
 *  onMentionReceived: Function,
    onChannelChanged: Function,
    onChannelDeleted: Function,
    onChannelFrozen: Function,
    onChannelUnfrozen: Function,
    onMetaDataCreated: Function,
    onMetaDataUpdated: Function,
    onMetaDataDeleted: Function,
    onMetaCountersCreated: Function,
    onMetaCountersUpdated: Function,
    onMetaCountersDeleted: Function,
    onChannelHidden: Function,
    onUserReceivedInvitation: Function,
    onUserDeclinedInvitation: Function,
    onUserJoined: Function,
    onUserLeft: Function,
    onDeliveryReceiptUpdated: Function,
    onReadReceiptUpdated: Function,
    onTypingStatusUpdated: Function,
    onUserEntered: Function,
    onUserExited: Function,
    onUserMuted: Function,
    onUserUnmuted: Function,
    onUserBanned: Function,
    onUserUnbanned: Function,
 * }} ChannelHandler - Option Handlers
 */
export function useChannelHandler(appId, userId, ChannelHandler) {
  const sb = new SendBird({ appId });
  sb.connect(userId);

  const keyParams = Object.keys(ChannelHandler);

  React.useEffect(() => {
    const groupHandlers = new sb.ChannelHandler();

    GroupChannel.map((event) => {
      if (keyParams.includes(event)) {
        groupHandlers[event] = ChannelHandler[event];
      } else {
        groupHandlers[event] = () => {};
      }
    });

    sb.addChannelHandler('different_handlers_with_hook_group', groupHandlers);
    return () => sb.removeChannelHandler('different_handlers_with_hook_group');
  });
}

export default function Handlers(props) {
  const { appId, userId } = props;
  const sb = new SendBird({ appId });

  React.useEffect(() => {
    sb.connect(userId);
  }, []);

  const keyProps = Object.keys(props);

  // Group Channel
  React.useEffect(() => {
    const groupHandlers = new sb.ChannelHandler();
    // eslint-disable-next-line array-callback-return
    GroupChannel.map((event) => {
      if (keyProps.includes(event)) {
        groupHandlers[event] = props[event];
      } else {
        groupHandlers[event] = () => {};
      }
    });

    sb.addChannelHandler('different_handlers_group', groupHandlers);
    return () => sb.removeChannelHandler('different_handlers_group');
  });

  React.useEffect(() => {
    const usersHandlers = new sb.UserEventHandler();

    UsersHandlers.map((event) => {
      if (keyProps.includes(event)) {
        usersHandlers[event] = props[event];
      } else {
        usersHandlers[event] = () => {};
      }
    });

    sb.addUserEventHandler('different_handlers_users', usersHandlers);

    return () => sb.removeUserEventHandler('different_handlers_users');
  });

  React.useEffect(() => {
    const connectionHandlers = new sb.ConnectionHandler();

    ConnectionHandlers.map((event) => {
      if (keyProps.includes(event)) {
        connectionHandlers[event] = props[event];
      } else {
        connectionHandlers[event] = () => {};
      }
    });
  });

  return null;
}

Handlers.propTypes = {
  appId: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageReceived: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageUpdated: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMessageDeleted: PropTypes.func,
  /**
   * function on receive a new message
   * @param {channel} - information with channel
   * @param {message} - information with message
   */
  onMentionReceived: PropTypes.func,
  onChannelChanged: PropTypes.func,
  onChannelDeleted: PropTypes.func,
  onChannelFrozen: PropTypes.func,
  onChannelUnfrozen: PropTypes.func,
  onMetaDataCreated: PropTypes.func,
  onMetaDataUpdated: PropTypes.func,
  onMetaDataDeleted: PropTypes.func,
  onMetaCountersCreated: PropTypes.func,
  onMetaCountersUpdated: PropTypes.func,
  onMetaCountersDeleted: PropTypes.func,
  onChannelHidden: PropTypes.func,
  onUserReceivedInvitation: PropTypes.func,
  onUserDeclinedInvitation: PropTypes.func,
  onUserJoined: PropTypes.func,
  onUserLeft: PropTypes.func,
  onDeliveryReceiptUpdated: PropTypes.func,
  onReadReceiptUpdated: PropTypes.func,
  onTypingStatusUpdated: PropTypes.func,
  onUserEntered: PropTypes.func,
  onUserExited: PropTypes.func,
  onUserMuted: PropTypes.func,
  onUserUnmuted: PropTypes.func,
  onUserBanned: PropTypes.func,
  onUserUnbanned: PropTypes.func,
  onReconnectStarted: PropTypes.func,
  onReconnectSucceeded: PropTypes.func,
  onReconnectFailed: PropTypes.func,
  onFriendsDiscovered: PropTypes.func,
  onTotalUnreadMessageCountUpdated: PropTypes.func,
};
