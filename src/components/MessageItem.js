/* eslint-disable max-len */
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
import _ from 'lodash';
import { rupiah } from '../tools/helpers';

import Img from './Img';

/**
 * @param {{
 *  message,
 *  messageId,
 *  messageTime,
 *  messageData,
 *  customType,
 *  isUser,
 * }} props - message text
 * @return {function}
 */
function MessageItem(props) {
  const {
    message,
    messageId,
    messageTime,
    messageData,
    sender,
    customType,
    isUser,
  } = props;

  const isInvoice = customType === 'invoice';
  const isProduct = customType === 'product';

  const paramData = messageData && JSON.parse(messageData);

  const classNameMessageItem = classnames('w-full flex flex-col justify-end', {
    'items-start': !isUser,
    'items-end': isUser,
  });

  const classNameFooter = classnames('flex', {
    'justify-start ml-3 -mt-2': !isUser,
    'justify-end mr-3 -mt-2': isUser,
  });

  const classNameMessageContent = classnames('text-xs font-hind p-2 mb-3', {
    'rounded-r-lg rounded-tl-lg relative beautymates__reply__other beautymates__message__other': !isUser,
    'rounded-l-lg rounded-tr-lg relative beautymates__reply_colors beautymates__message__user': isUser,
  });

  const classNameContentBox = classnames('flex flex-col', {
    'ml-5': !isUser,
    'mr-0': isUser,
  });

  const classNameThumbnailProfile = classnames('w-10 h-10 rounded-full object-cover', {
    hidden: isUser,
    block: !isUser,
  });

  const classNameProductContent = classnames('beautymates__reply_colors flex p-2 my-1 shadow-md rounded-lg', {
    hidden: !isProduct,
    block: isProduct,
  });

  const classNameInvoiceContent = classnames('beautymates__reply_colors flex p-2 my-1 shadow-md', {
    hidden: !isInvoice,
    block: isInvoice,
  });

  const classNameMessageCustom = classnames('flex flex-col', {
    'ml-5': !isUser,
    'mr-0': isUser,
  });

  return (
    <div className={classNameMessageItem} key={messageId}>
      <div className={classNameMessageCustom}>
        <div className={classNameProductContent}>
          <Img
            alt="content"
            src={_.get(paramData, 'image')}
            className="w-16 h-16 rounded-md object-cover"
          />
          <div className="flex flex-col ml-2">
            <a href={_.get(paramData, 'url')} target="_blank" rel="noreferrer" className="text-md font-hind font-semibold truncate" style={{ maxWidth: 200 }}>
              {_.get(paramData, 'name')}
            </a>
            <span className="text-sm font-hind font-medium">
              {_.get(paramData, 'variant.label')}
            </span>
            <span className="text-sm font-hind font-medium text-bm-900">
              {paramData && paramData.price && rupiah(_.get(paramData, 'price'))}
            </span>
          </div>
        </div>
        <div className={classNameInvoiceContent}>
          <Img
            alt="content"
            src={_.get(paramData, 'image')}
            className="w-12 h-12 rounded-md object-cover"
          />
          <div className="flex flex-col ml-2">
            <a href={_.get(paramData, 'url')} target="_blank" rel="noreferrer" className="text-sm font-hind font-semibold" style={{ maxWidth: 150 }}>
              {_.get(paramData, 'noInvoice')}
            </a>
            <div className="capitalize font-medium text-sm truncate" style={{ maxWidth: 150 }}>
              {_.get(paramData, 'status')}
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-col py-2">
        <div className={classNameContentBox}>
          <p className={classNameMessageContent}>
            {message}
          </p>
          <div className={classNameFooter}>
            <span className="font-hind-siliguri" style={{ fontSize: 12, color: '#888888' }}>
              {moment(messageTime).format('HH:mm A')}
            </span>
          </div>
        </div>
        <Img
          alt="content"
          src={_.get(sender, 'profileUrl')}
          className={classNameThumbnailProfile}
        />
      </div>
    </div>
  );
}

MessageItem.propTypes = {
  message: PropTypes.string,
  messageId: PropTypes.number,
  messageTime: PropTypes.number,
  messageData: PropTypes.string,
  sender: PropTypes.object,
  customType: PropTypes.string,
  date: PropTypes.string,
  userId: PropTypes.string,
  isUser: PropTypes.bool,
};

export default MessageItem;
