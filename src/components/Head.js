import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import moment from 'moment';

import Img from './Img';

/**
 * 
 * @param {{currentChannel: object, userId: string}} props 
 */
function Head(props) {
  const { currentChannel, userId } = props;

  if (!currentChannel) {
    return (
      <div className="w-full flex items-center justify-between pt-2 pb-4 px-4 border-b-2 border-gray-100">
        <div className="flex items-center">
          <div className="w-10 h-10 rounded-full bg-gray-300" />
          <div className="flex flex-col ml-2">
            <div className="bg-gray-300 w-24 h-4 rounded" />
            <div className="bg-gray-300 w-20 h-2 mt-1 rounded" />
          </div>
        </div>
      </div>
    )
  }

  const listMembers = _.get(currentChannel, 'members');

  let users;
  _.map(listMembers, function (member, inMembers) {
    if (member.userId !== userId) {
      users = member;
    }
  });
  const lastTime = new moment(users.lastSeenAt).fromNow();
  
  return (
    <div className="w-full flex items-center justify-between pt-2 pb-4 px-4 border-b-2 border-white">
      <div className="flex items-center">
        <Img
          alt="channel-url"
          src={_.get(users, 'profileUrl')}
          className="w-12 h-12 rounded-full"
          lazyload={false}
        />
        <div className="flex flex-col ml-4">
          <span className="text-md font-hind font-semibold capitalize">
            {_.get(users, 'nickname')}
          </span>

          {_.get(users, 'connectionStatus') === "offline" ? (
            <span className="text-xs text-black">
              Offline
            </span>
          ) : (
              <div className="flex items-center">
                <span className="text-sm text-green-500 mr-1">
                  <svg
                    t="1595902622935"
                    className="icon w-3 h-3 fill-current"
                    viewBox="0 0 1024 1024"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                    p-id="1523"
                  >
                    <path
                      d="M512 298.666667c117.333333 0 213.333333 96 213.333333 213.333333s-96 213.333333-213.333333 213.333333-213.333333-96-213.333333-213.333333S394.666667 298.666667 512 298.666667z"
                      p-id="1524"
                    />
                  </svg>
                </span>
                <span className="text-xs text-back font-medium">
                  Active
                </span>
              </div>
            )}
        </div>
      </div>
      <div className="flex">
        <button className="focus:outline-none">
          <svg t="1596951833942" className="icon w-6 h-6 fill-current text-black transform rotate-90" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2054" width="200" height="200"><path d="M704 512c0-35.35 28.65-64 64-64s64 28.65 64 64-28.65 64-64 64-64-28.65-64-64z m-256 0c0-35.35 28.65-64 64-64s64 28.65 64 64-28.65 64-64 64-64-28.65-64-64z m-256 0c0-35.35 28.65-64 64-64s64 28.65 64 64-28.65 64-64 64-64-28.65-64-64z" p-id="2055"></path></svg>
        </button>
      </div>
    </div>
  );
}

Head.propTypes = {
  currentChannel: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  userId: PropTypes.string,
};

export default Head;
