/* eslint-disable max-len */
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
import _ from 'lodash';

import Img from './Img';

/**
 * Get Channel List
 * @param {{
 *  channelList: array,
 *  setCurrentChannel: function,
 *  userId: string,
 * }} props
 * @return {function}
 */
function ChannelList(props) {
  const {channelList, isLoad, setCurrentChannel, userId} = props;
  const [active, setActive] = React.useState(null);
  // eslint-disable-next-line max-len
  const classChannelList = classnames('flex flex-col overflow-y-auto beautymates__scrooll mt-4');

  if (isLoad) {
    return (
      <div className="flex justify-center my-4">
        <svg
          viewBox="0 0 1024 1024"
          focusable="false"
          className="animate-spin h-10 w-10 text-bm-500"
          data-icon="loading"
          width="1em"
          height="1em"
          fill="currentColor"
          aria-hidden="true"
        >
          <path d="M988 548c-19.9 0-36-16.1-36-36 0-59.4-11.6-117-34.6-171.3a440.45 440.45 0 0 0-94.3-139.9 437.71 437.71 0 0 0-139.9-94.3C629 83.6 571.4 72 512 72c-19.9 0-36-16.1-36-36s16.1-36 36-36c69.1 0 136.2 13.5 199.3 40.3C772.3 66 827 103 874 150c47 47 83.9 101.8 109.7 162.7 26.7 63.1 40.2 130.2 40.2 199.3.1 19.9-16 36-35.9 36z"></path>
        </svg>
      </div>
    );
  }

  if (channelList.length === 0) {
    return (
      <div className="w-full flex justify-center py-6">
        <span className="font-hind-siliguri text-sm font-semibold text-black">
          There is no message channel.
        </span>
      </div>
    );
  }

  return (
    <ul className={classChannelList}>
      {_.map(channelList, (channelParam, channelkey) => {
        const listMembers = _.get(channelParam, 'members');
        const rejectUser = _.reject(listMembers, function(o) {
          return o.userId === userId;
        });
        const users = rejectUser[0];
        const isActive = active === channelkey;
        // eslint-disable-next-line max-len
        const classChannelItem = classnames('flex py-4 px-3 cursor-pointer bg-white mb-2 transition duration-150 ease-in-out', {
          'beautymates__channel__active': isActive,
          'bg-white': !isActive,
        });

        return (
          <li
            className={classChannelItem}
            onClick={() => {
              setCurrentChannel(channelParam);
              setActive(channelkey);
            }}
            key={channelkey}
          >
            <Img
              alt="channel-list"
              src={_.get(users, 'profileUrl')}
              className="w-10 h-10 rounded-full"
              lazyload={false}
            />
            <div className="flex flex-col ml-2 w-full">
              <div className="flex items-center justify-between w-full">
                <span
                  // eslint-disable-next-line max-len
                  className="text-sm font-semibold font-hind whitespace-no-wrap truncate capitalize"
                  style={{maxWidth: 125}}
                >
                  {_.get(users, 'nickname')}
                </span>
                <div className="flex items-center">
                  {_.get(users, 'connectionStatus') === 'online' ? (
                    <div
                      style={{width: 8, height: 8}}
                      className="bg-green-500 rounded-full"
                    />
                  ) : (
                      <span className="font-hind text-xs">
                        {moment(_.get(users, 'lastSeenAt')).format('HH:mm')}
                      </span>
                    )}
                </div>
              </div>
              <div className="flex items-center w-full">
                <p className="text-xs font-hind font-normal beautymates__last_message">
                  {_.get(channelParam, 'lastMessage.message')}
                </p>
                {_.get(channelParam, 'lastMessage.messageType') === 'file' && (
                  <p className="text-xs font-hind text-black font-semibold">
                   a new file has received
                  </p>
                )}
              </div>
            </div>
          </li>
        );
      })}
    </ul>
  );
}

ChannelList.propTypes = {
  channelList: PropTypes.array.isRequired,
  setCurrentChannel: PropTypes.func,
  isLoad: PropTypes.bool,
  userId: PropTypes.string,
};

export default ChannelList;
