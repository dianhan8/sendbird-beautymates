import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
 
import Img from './Img';

function MessageImage(props) {
  const { isUser, src, alt, messageTime } = props;

  const classContainer = classnames('w-full flex my-2', {
    'justify-start': !isUser,
    'justify-end': isUser,
  });

  const classContent = classnames('flex flex-col beautymates__reply_colors shadow-md rounded-md px-3 py-3');
  const classNameFooter = classnames('flex', {
    'justify-start': !isUser,
    'justify-end': isUser,
  });

  return (
    <div className={classContainer}>
      <div className={classContent}>
        <Img
          className="rounded-md"
          alt={alt}
          src={src}
          lazyload={false}
        />
        <div className={classNameFooter}>
          <span className="text-xs hind-siliguri-siliguri">
            {moment(messageTime).format('HH:mm A')}
          </span>
        </div>
      </div>
    </div>
  )
}

export default MessageImage;