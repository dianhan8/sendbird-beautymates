import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import classnames from 'classnames';
import { rupiah } from './../tools/helpers';
import Img from './Img';

function Preview(props) {
  const { customData, visible, setCustomData } = props;

  const [show, setShow] = React.useState(false);

  React.useEffect(() => {
    if (customData !== null) {
      setShow(true);
    }
  }, [customData])

  React.useState(() => {
    setShow(visible);
  }, [visible]);

  if (customData === null) {
    return <div />
  }

  const classNameProduct = classnames('relative w-full my-2 px-4 py-4 beautymates__reply_colors rounded-md flex items-start', {
    'hidden': !show,
    'block': show,
  });

  const classNameInvoice = classnames('relative w-full my-2 px-4 py-4 beautymates__reply_colors rounded-md', {
    'hidden': !show,
    'block': show,
  });

  const typeData = get(customData, 'typeData');

  if (typeData === 'product') {
    return (
      <div className={classNameProduct}>
        <Img
          alt="content"
          src={_.get(customData, 'image')}
          className="w-16 h-16 rounded-md object-cover"
        />
        <div className="flex flex-col ml-2">
          <a href={_.get(customData, 'url')} target="_blank" className="text-md font-hind font-semibold truncate" style={{ maxWidth: 150 }}>
            {_.get(customData, 'name')}
          </a>
          <span className="text-sm font-hind">
            {_.get(customData, 'variant.label')}
          </span>
          <span className="text-sm font-hind font-medium text-bm-900">
            {customData && customData.price && rupiah(_.get(customData, 'price'))}
          </span>
        </div>
        <button
          className="absolute mt-3 mr-3 right-0 top-0 py-2 px-2 rounded-full focus:outline-none"
          onClick={() => {
            setShow(false);
            setCustomData(null);
          }}>
          <svg t="1600318550542" className="w-5 h-5" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2493" width="200" height="200"><path d="M682.98 728.11L512.32 557.44 341.43 728.11c-12.5 12.56-32.82 12.61-45.38 0.11s-12.61-32.82-0.11-45.38l170.88-170.67-170.86-171.09c-12.5-12.5-12.5-32.77 0-45.27s32.77-12.5 45.27 0L512.1 466.69l170.67-170.88c12.5-12.5 32.77-12.5 45.27 0s12.5 32.77 0 45.27L557.38 511.96l170.67 170.67c12.5 12.5 12.5 32.77 0 45.27s-32.77 12.5-45.27 0l0.2 0.21z" p-id="2494"></path></svg>
        </button>
      </div>
    )
  }

  if (typeData === 'invoice') {
    return (
      <div className={classNameInvoice}>
        <a href={get(customData, 'url')} target="_blank" className="flex items-start">
          <Img lazyload src={get(customData, 'image')} className="w-12 h-12 object-cover" />
          <div className="flex flex-col ml-3">
            <span className="font-hind font-semibold text-sm">{get(customData, 'noInvoice')}</span>
            <div className="font-hind text-sm capitalize">
              {get(customData, 'status')}
            </div>
          </div>
        </a>
        <button className="absolute mt-3 mr-3 right-0 top-0 py-2 px-2 rounded-full focus:outline-none" onClick={() => setShow(false)}>
          <svg t="1600318550542" className="w-5 h-5" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2493" width="200" height="200"><path d="M682.98 728.11L512.32 557.44 341.43 728.11c-12.5 12.56-32.82 12.61-45.38 0.11s-12.61-32.82-0.11-45.38l170.88-170.67-170.86-171.09c-12.5-12.5-12.5-32.77 0-45.27s32.77-12.5 45.27 0L512.1 466.69l170.67-170.88c12.5-12.5 32.77-12.5 45.27 0s12.5 32.77 0 45.27L557.38 511.96l170.67 170.67c12.5 12.5 12.5 32.77 0 45.27s-32.77 12.5-45.27 0l0.2 0.21z" p-id="2494"></path></svg>
        </button>
      </div>
    )
  }
}

Preview.propTypes = {
  customeData: PropTypes.object,
  visible: PropTypes.bool,
};

Preview.defaultProps = {
  visible: true,
}

export default Preview;
