/* eslint-disable max-len */
import React from 'react';
import PropTypes from 'prop-types';
import Sendbird from 'sendbird';
import {useForm} from 'react-hook-form';
import classnames from 'classnames';

import {convertFileToBase64} from './../tools/helpers';

/**
 * 
 * @param {{
 * currentChannel: object,
 * handleSubmit: function,
 * setFile: function
 * }} props
 * @return {node}
 */
function Input(props) {
  const {currentChannel, setMessageList, setFile } = props;
  const {register, handleSubmit, reset, watch} = useForm();

  const [errors, setError] = React.useState();
  const [visible, setVisible] = React.useState(false);
  const [isLoad, setLoad] = React.useState(false);
  const [preview, setPreview] = React.useState(null);

  const refContainers = React.useRef();
  const refInputFile = React.useRef();
  const fileMessage = watch('fileMessage');

  React.useEffect(() => {
    if (fileMessage) {
      setFile(fileMessage);
      setVisible(false);
    }
  }, [fileMessage]);

  React.useEffect(() => {
    window.addEventListener('mousedown', handleClose);
    return window.addEventListener('mousedown', handleClose);
  });

  /**
   *
   * @param {node} event
   */
  function handleClose(event) {
    // eslint-disable-next-line max-len
    if (refContainers.current && !refContainers.current.contains(event.target)) {
      setVisible(false);
    }
  }

  /**
   * handle click input
   */
  function handleClick() {
    refInputFile.current.click();
  }

  /**
   * Submit function
   * @param {object} e
   */
  function onSubmit(e) {
    setLoad(true);
    const sb = new Sendbird.getInstance();
    const isEmptyFile = _.size(e.fileMessage) === 0;
    if (!isEmptyFile) {
      const paramsFile = new sb.FileMessageParams();
      paramsFile.file = e.fileMessage[0];
      paramsFile.fileName = e.fileMessage[0].name;
      paramsFile.fileSize = e.fileMessage[0].size;
      paramsFile.thumbnailSizes = [{maxWidth: 100, maxHeight: 100}, {maxWidth: 200, maxHeight: 200}]; // Add the maximum sizes of thumbnail images (allowed number of thumbnail images: 3).
      paramsFile.mentionType = 'users'; // Either 'users' or 'channel'
      paramsFile.pushNotificationDeliveryOption = 'default'; // Either 'default' or 'suppress'

      currentChannel.sendFileMessage(paramsFile, function(fileMessage, errorFile) {
        if (errorFile) {
          return setError(errorFile);
        }

        setMessageList((prev) => [...prev, fileMessage]);
        setFile(null);
        return reset();
      });
    }

    const params = new sb.UserMessageParams();
    params.message = e.messageText;

    currentChannel.sendUserMessage(params, function(message, error) {
      if (error) {
        console.log(error);
        return setError(error);
      }

      setMessageList((prev) => [...prev, message]);
      setLoad(false);
      return reset();
    });
  }

  const classNameInput = classnames('absolute flex items-center bg-white rounded shadow-lg bottom-0 right-0 mb-16 mr-2 transition-all duration-150 transform', {
    'scale-100 ease-in block': visible,
    'scale-0 ease-out hidden': !visible,
  });

  return (
    <div className="w-full py-4 px-5">
      <form className="w-full flex items-center" onSubmit={handleSubmit(onSubmit)}>
        <div className="relative inline-block w-full">
          <input
            name="messageText"
            type="text"
            placeholder="Write text something"
            className="py-4 px-6 w-full bg-white text-md font-hind rounded-full focus:outline-none focus:shadow-outline"
            style={{color: '#888888'}}
            onChange={() => currentChannel.startTyping()}
            onBlur={() => currentChannel.endTyping()}
            ref={register({required: true})}
          />
          <div className="absolute right-0 bottom-0 top-0 mr-2" style={{marginBottom: 'auto', marginTop: 12}}>
            <div className="relative" ref={refContainers}>
              <button type="button" className="rounded-full focus:outline-none text-bm p-2 my-auto bg-bm-900 text-bm mr-1" onClick={() => setVisible((prev) => !prev)}>
                <svg t="1597333153584" className="w-4 h-4 fill-current text-white" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4336" width="200" height="200"><path d="M511 68c16.506 0 29.887 13.38 29.887 29.887l-0.001 386.226h386.227C943.62 484.113 957 497.494 957 514s-13.38 29.887-29.887 29.887l-386.227-0.001v386.227C540.887 946.62 527.507 960 511 960s-29.887-13.38-29.887-29.887V543.886H94.887C78.38 543.887 65 530.507 65 514s13.38-29.887 29.887-29.887h386.226V97.887C481.113 81.38 494.494 68 511 68z" fill="#ffffff" p-id="4337"></path></svg>
              </button>
              <div className={classNameInput}>
                <button className="px-2 py-2">
                  <svg t="1597332003868" className="icon w-8 h-8" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4200" width="200" height="200"><path d="M682.47 101c72.01 0 130.041 59.663 131.194 133.157l0.018 2.231v467.066c0 16.519-13.383 29.91-29.891 29.91-16.273 0-29.509-13.012-29.883-29.204l-0.009-0.706V236.388c0-41.574-31.608-74.895-70.255-75.557l-1.174-0.01H202.212c-38.787 0-70.784 32.905-71.42 74.31l-0.01 1.257v601.224c0 41.574 31.609 74.895 70.256 75.557l1.174 0.01h642.917c21.34 0 38.686-17.111 39.082-38.37l0.007-0.743V199.292c0-16.52 13.382-29.91 29.89-29.91 16.273 0 29.509 13.01 29.892 29.204v675.48c0 54.093-43.385 98.047-97.236 98.92l-1.635 0.014H202.212c-72.01 0-130.041-59.663-131.195-133.157L71 837.612V236.388c0-73.768 57.311-134.17 129.034-135.37l2.178-0.018H682.47z m-78.027 616.612c12.7 0 22.994 10.301 22.994 23.008 0 12.492-9.949 22.658-22.35 23l-0.644 0.008H289.436c-12.699 0-22.993-10.301-22.993-23.008 0-12.491 9.948-22.658 22.35-22.999l0.643-0.009h315.007z m0-138.047c12.7 0 22.994 10.3 22.994 23.008 0 12.491-9.949 22.658-22.35 22.999l-0.644 0.008H289.436c-12.699 0-22.993-10.3-22.993-23.007 0-12.492 9.948-22.658 22.35-23l0.643-0.008h315.007z m0-138.048c12.7 0 22.994 10.301 22.994 23.008 0 12.492-9.949 22.658-22.35 23l-0.644 0.008H289.436c-12.699 0-22.993-10.301-22.993-23.008 0-12.491 9.948-22.658 22.35-22.999l0.643-0.009h315.007z m-75.876-132.181c12.7 0 22.994 10.3 22.994 23.008 0 12.491-9.949 22.658-22.35 22.999l-0.644 0.009H366.343c-12.699 0-22.993-10.301-22.993-23.008 0-12.492 9.948-22.658 22.35-23l0.643-0.008h162.224z" fill="#333333" p-id="4201"></path></svg>
                </button>
                <button className="px-2 py-2">
                  <svg t="1597331687826" className="icon w-8 h-8" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4064" width="200" height="200"><path d="M503.297 73.86a29.927 29.927 0 0 1 29.406 0l360.089 203.131A29.89 29.89 0 0 1 908 303.024v416.952a29.89 29.89 0 0 1-15.208 26.033l-360.089 203.13a29.927 29.927 0 0 1-29.406 0L143.208 746.01A29.89 29.89 0 0 1 128 719.976V303.024a29.89 29.89 0 0 1 15.208-26.033zM518 134.219L187.823 320.475v382.047L518 888.778 848.177 702.52V320.475L518 134.218zM762.82 335.63c14.286-7.588 32.099-2.435 40.085 11.731l0.36 0.656c7.592 14.278 2.436 32.08-11.739 40.061l-240.168 135.22v280.236l-0.009 0.705c-0.375 16.184-13.62 29.188-29.903 29.188-0.384 0-0.767-0.007-1.147-0.022-0.381 0.015-0.764 0.022-1.149 0.022-16.283 0-29.528-13.004-29.903-29.188l-0.008-0.705V523.299L249.07 388.079c-14.174-7.981-19.33-25.784-11.738-40.062l0.36-0.656c7.985-14.166 25.799-19.319 40.085-11.73l0.657 0.358 241.864 136.177 241.865-136.177z" fill="#333333" p-id="4065"></path></svg>
                </button>
                <button type="button" className="px-2 py-2 focus:outline-none" onClick={() => handleClick()}>
                  <svg t="1597331143764" className="icon w-8 h-8" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3928" width="200" height="200"><path d="M717.344 892.2c16.52 0 29.914 13.387 29.914 29.9 0 16.277-13.013 29.517-29.208 29.892l-0.706 0.008H307.557c-16.52 0-29.913-13.387-29.913-29.9 0-16.277 13.012-29.517 29.207-29.892l0.706-0.008h409.787zM724.045 78C791.399 78 846 132.576 846 199.9v517.5c0 67.324-54.601 121.9-121.955 121.9h-421.09C235.601 839.3 181 784.724 181 717.4V199.9C181 132.576 235.601 78 302.955 78z m-333.06 427.254l-0.638 0.53-143.921 122.62a30.009 30.009 0 0 1-5.6 3.777l0.001 85.219c0 34.297 27.816 62.1 62.128 62.1h421.09a62.77 62.77 0 0 0 7.759-0.48L436.127 506.67c-12.636-11.64-31.87-12.192-45.143-1.416z m258.684-113.839l-0.61 0.58L527.9 509.882l252.859 232.91a61.818 61.818 0 0 0 5.406-24.366l0.008-1.027V479.357a29.975 29.975 0 0 1-4.408-3.442l-0.564-0.546-83.744-83.133c-13.165-13.068-34.26-13.368-47.788-0.821zM724.045 137.8h-421.09c-34.312 0-62.128 27.803-62.128 62.1l-0.001 354.698 110.711-94.325c35.957-30.634 88.965-29.922 124.074 1.46l1.059 0.961 7.171 6.606 123.487-120.156c36.521-35.536 94.584-35.602 131.185-0.415l1.103 1.077 46.557 46.218V199.9c0-33.954-27.262-61.543-61.1-62.092l-1.028-0.008z m-319.844 48.3c53.372 0 96.643 43.252 96.643 96.6 0 53.348-43.271 96.6-96.643 96.6-53.373 0-96.644-43.252-96.644-96.6 0-53.348 43.271-96.6 96.644-96.6z m0 46c-27.956 0-50.623 22.657-50.623 50.6s22.667 50.6 50.623 50.6c27.955 0 50.623-22.657 50.623-50.6s-22.668-50.6-50.623-50.6z" fill="#333333" p-id="3929"></path></svg>
                </button>
              </div>
            </div>
          </div>
        </div>
        <input
          name="fileMessage"
          id="messageFile"
          type="file"
          accept="image/x-png,image/gif,image/jpeg"
          className="hidden"
          ref={(el) => {
            refInputFile.current = el;
            register(el);
          }}
        />
        <div className="flex items-center justify-center ml-2">
          <button type="submit" className="p-2 bg-bm-900 rounded-full focus:outline-none">
            {isLoad ? (
              <svg t="1597917609107" className="icon w-8 h-8 animate-spin" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4599" width="200" height="200"><path d="M490 70c140.5 0 269.31 65.56 352.323 172.436l-0.006-110.364c0-16.506 13.38-29.886 29.887-29.886 16.27 0 29.503 13 29.878 29.18l0.008 0.706v211.505c0 8.396-3.462 15.983-9.036 21.412-5.278 5.426-12.598 8.853-20.72 9.041l-0.705 0.009H660.124c-16.506 0-29.887-13.381-29.887-29.887 0-16.27 13.001-29.504 29.181-29.878l0.706-0.009 159.311-0.002C750.079 201.113 626.416 129.773 490 129.773c-213.307 0-386.227 172.92-386.227 386.227S276.693 902.227 490 902.227c211.174 0 382.764-169.479 386.175-379.84l0.052-6.387c0-16.506 13.38-29.887 29.886-29.887S936 499.494 936 516c0 246.319-199.681 446-446 446S44 762.319 44 516 243.681 70 490 70z" fill="#ffffff" p-id="4600"></path></svg>
            ):(
              <svg t="1596950655072" className="icon w-8 h-8 fill-current text-white" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3773" width="200" height="200"><path d="M85.333333 896l896-384L85.333333 128v298.666667l640 85.333333-640 85.333333v298.666667z" fill="" p-id="3774"></path></svg>
            )}
          </button>
        </div>
      </form>
    </div>
  );
}

Input.propTypes = {
  currentChannel: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  setMessageList: PropTypes.func,
  setFile: PropTypes.func,
};

export default Input;
