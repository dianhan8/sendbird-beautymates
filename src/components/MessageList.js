import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Sendbird from 'sendbird';
import moment from 'moment';
import _, { set } from 'lodash';

import classnames from 'classnames';

import Img from './Img';
import Head from './Head';
import MessageItem from './MessageItem';
import Input from './Input';
import MessageImage from './MessageImage';

function MessageList(props) {
  const { currentChannel, messageList, setMessageList, userId, isTyping } = props;
  const [file, setFile] = React.useState(null);

  function File() {
    if (_.size(file) === 1) {
      return (
        <div className="w-full flex justify-end py-4 bg-bm-100 transition-all duration-150 ease">
          <p className="bg-white py-2 px-4 rounded-md hind-siliguri text-sm">
            {_.get(file, '[0].name')}
          </p>
          <button
            type="button"
            className="py-2 px-2 focus:outline-none"
            onClick={() => setFile(null)}
          >
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="25" height="25"><path className="heroicon-ui" d="M16.24 14.83a1 1 0 0 1-1.41 1.41L12 13.41l-2.83 2.83a1 1 0 0 1-1.41-1.41L10.59 12 7.76 9.17a1 1 0 0 1 1.41-1.41L12 10.59l2.83-2.83a1 1 0 0 1 1.41 1.41L13.41 12l2.83 2.83z" /></svg>
          </button>
        </div>
      )
    }

    return <div />
  }

  const classNameTyping = classnames('rounded-r-lg rounded-tl-lg relative w-full flex flex-col pl-5 justify-end items-start transition-all duration-300', {
    'invisible h-0 opacity-0': !isTyping,
    'visible h-full opacity-100': isTyping,
  });

  return (
    <React.Fragment>
      <Head currentChannel={currentChannel} userId={userId} />
      <div className="w-full flex flex-col">
        <div className="py-4 pr-3 mt-3 flex flex-col-reverse overflow-y-auto beautymates__scrooll" style={{ minHeight: 560, maxHeight: 560 }}>
          {/* <div className="w-full">
            <span className="text-sm hind-siliguri-siliguri px-3 beautymates__reply_colors rounded-full">
              is typing....
            </span>
          </div> */}
          <div className="flex flex-col">
            {_.map(messageList, (messageParam, messagekey) => {
              const prev = messageList[messagekey - 1];
              const prevCreatedAt = prev && prev.createdAt;
              const currentCreatedAt = messageParam.createdAt;
              const hasSeparator = !(prevCreatedAt && moment(currentCreatedAt).isSame(moment(prevCreatedAt), 'day'));
              const isUser = _.get(messageParam, '_sender.userId') === userId;

              return (
                <React.Fragment key={messagekey}>
                  {hasSeparator && (
                    <div className="beautymates__divider">
                      <span className="beautymates__divider_text">{moment(messageParam.createdAt).format('DD MMMM YYYY')}</span>
                    </div>
                  )}
                  {messageParam.messageType === "user" && (
                    <MessageItem
                      message={_.get(messageParam, 'message')}
                      messageTime={_.get(messageParam, 'createdAt')}
                      messageId={_.get(messageParam, 'messageId')}
                      messageData={_.get(messageParam, 'data')}
                      sender={_.get(messageParam, 'sender')}
                      customType={_.get(messageParam, 'customType')}
                      isUser={isUser}
                    />
                  )}
                  {messageParam.messageType === "file" && (
                    <MessageImage
                      src={_.get(messageParam, 'thumbnails[1].url')}
                      alt={_.get(messageParam, 'name')}
                      messageTime={_.get(messageParam, 'createdAt')}
                      isUser={isUser}
                    />
                  )}
                </React.Fragment>
              )
            })}
            <div className={classNameTyping}>
              <p className="text-xs font-hind p-2 beautymates__reply__other font-semibold px-4 rounded-r-lg rounded-tl-lg">
                Someone is Typing
              </p>
            </div>
            <File />
          </div>
        </div>
        <Input
          currentChannel={currentChannel}
          setMessageList={setMessageList}
          setFile={fileMessage => setFile(fileMessage)}
        />
      </div>
    </React.Fragment>
  )
}

MessageList.propTypes = {
  currentChannel: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  messageList: PropTypes.array,
  setMessageList: PropTypes.func,
  userId: PropTypes.string,
};

export default memo(MessageList);