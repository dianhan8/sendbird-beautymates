import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import errorImg from './../assets/error.png';

/**
 * Image
 * @param {{alt: string, src: string, className: string}} props 
 */
export default function Img(props) {
  const { alt, src, className, lazyload } = props;

  function imageError(ev) {
    ev.target.src = errorImg;
    ev.target.onerror = null;
  }

  const classNameImage = classnames({
    'lazyload': lazyload,
  }, className);

  if (lazyload) {
    return (
      <img
        alt={alt}
        src={errorImg}
        data-src={src}
        className={classNameImage}
        onError={e => imageError(e)}
      />
    )
  } else {
    return (
      <img
        alt={alt}
        src={src}
        className={classNameImage}
        onError={e => imageError(e)}
      />
    )
  }


}

Img.propTypes = {
  alt: PropTypes.string,
  src: PropTypes.string,
  className: PropTypes.string,
  lazyload: PropTypes.bool,
};

Img.defaultProps = {
  lazyload: true,
}