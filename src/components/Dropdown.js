import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

function Dropdown(props) {
  const { triggerProp, children } = props;

  const [visible, setVisible] = React.useState(false);
  const refContainer = React.useRef(null);

  React.useEffect(() => {
    window.addEventListener('mousedown', handleClose);
    return window.addEventListener('mousedown', handleClose);
  });

  function handleClose(event) {
    if (refContainer.current && !refContainer.current.contains(event.target)){
      setVisible(false);
    }
  }

  const classNameMenu = classnames('absolute bg-white shadow right-0 mt-2', {
    'hidden': !visible,
    'block': visible,
  });

  return (
    <div className="relative" ref={refContainer}>
      <button
        type="button"
        className="transition ease duration-150 px-1 py-1 hover:shadow-md focus:shadow focus:outline-none"
        onClick={() => setVisible(prev => !prev)}
      >
        {triggerProp}
      </button>
      <ul className={classNameMenu} style={{ width: 100 }}>
        {children}
      </ul>
    </div>
  )
}

Dropdown.propTypes = {
  triggerProp: PropTypes.node.isRequired,
  children: PropTypes.node,
};

export default Dropdown;
