import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Messaging from './Messaging';
import Headlers, { useChannelHandler } from './Handlers';
import Sendbird from 'sendbird';

/**
 * Preview Chatting
 * @return {function}
 */
function Public() {
  const [visible, setVisible] = React.useState(false);
  const [users, setUsers] = React.useState({ userId: null, otherUser: null });
  const [withPage, setWithPage] = React.useState(true);
  const sb = new Sendbird({ appId: '82F255B2-2152-421F-9207-ADD35503FD9E' });

  function connectSb(){
    sb.connect('store_13', function (user, error) {
      if (error) {
        console.log(error);
      }
    })
  }

  const ChannelHandler = {
    onMessageReceived: (channel, message) => {
      console.log('Form Hook', message);
    }
  }

  return (
    <div>
      <div className="container mx-auto my-20">
        <div className="flex items-center">
          <button
            // eslint-disable-next-line max-len
            className="bg-red-500 font-hind font-semibold text-sm font-hind text-white rounded-lg px-4 py-2 focus:outline-none mr-2"
            onClick={() => connectSb()}
          >
            Connect
          </button>
          <button
            // eslint-disable-next-line max-len
            className="bg-red-500 font-hind font-semibold text-sm font-hind text-white rounded-lg px-4 py-2 focus:outline-none mr-2"
            onClick={() => sb.disconnect()}
          >
            Disconnect
          </button>

          <button
            // eslint-disable-next-line max-len
            className="bg-red-500 font-hind font-semibold text-sm font-hind text-white rounded-lg px-4 py-2 focus:outline-none"
            onClick={() => setWithPage(prev => !prev)}
          >
            With Chat Page ?
          </button>
        </div>
        {withPage && (
          <App
            appId="82F255B2-2152-421F-9207-ADD35503FD9E"
            userId="store_13"
            onMessageReceived={(channel, message) => {
              console.log(message);
            }}
          />
        )}
        <div className="my-6 w-full">
          <button
            // eslint-disable-next-line max-len
            className="bg-red-500 font-hind font-semibold text-sm font-hind text-white rounded-lg px-4 py-2 focus:outline-none"
            onClick={() => {
              setVisible((prev) => !prev);
              setUsers({ userId: 'store_13', otherUser: 'cus_21' });
            }}
          >
            Pop Up as Store
          </button>
          <button
            // eslint-disable-next-line max-len
            className="bg-red-500 font-hind font-semibold text-sm font-hind text-white rounded-lg px-4 py-2 focus:outline-none ml-4"
            onClick={() => {
              setVisible((prev) => !prev);
              setUsers({ userId: 'cus_21', otherUser: 'store_13' });
            }}
          >
            Pop Up as Customer
          </button>
        </div>
      </div>
      <Messaging
        appId="82F255B2-2152-421F-9207-ADD35503FD9E"
        // eslint-disable-next-line max-len
        urlChannel="sendbird_group_channel_24410906_238946735d445fcd60665c80724ec08a16c102f3"
        customData={{
          name: 'Snail Make Up',
          variant: { label: 'Shade' },
          image: 'https://api.beautymates.co.id/storage/gambar/Item/RYRyZJDFRXve0wm57tTjvtvG5whBlL8CBP40tDr1.jpeg',
          price: 50000,
          url: 'https://customerblanjania.kotadigivice.com',
          typeData: 'product',
        }}
        visible={visible}
        userId={users.userId}
        userIdStore={users.otherUser}
        onMessageReceived={(channel, message) => {
          console.log('As Customer =>', message);
        }}
        onMetaDataUpdated={(groupChannel) => {
          console.log(groupChannel, 'halo')
        }}

      />
    </div>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <Public />
  </React.StrictMode>,
  document.getElementById('root'),
);
