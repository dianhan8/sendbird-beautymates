<h2>Sendbird UI Beautymates<h2>
<p>for beautymates only</p>

## Quick start
 1. Run `npm install git+https://gitlab.com/dianhan8/sendbird-beautymates.git`.
 2. Import library `import SendbirdBeautymates from 'sendbird-beautymates';`

  Examples Code Use: </br>
  ```
  import React from 'react';
  import SendbirdBeautymates from 'sendbird-beautymates';

  export default App(){
    return (
      <div className="container">
        <SendbirdBeautymates
          appId="your-appId"
          userId="your-userId"
          limitMessage="your-limmit-mesaage"
        />
      </div>
    )
  }
  ```

## Note
- For now already have 3 props `appId, userId, limitMessage`, other customize still development.
- `limitMessage ` is not required but if you have limit message, use that.

## Contribute
- For contribute just merge request config name `Name Feature - [Name Branch]:[UserName]`


